
FROM  registry.cern.ch/docker.io/library/centos:centos7

RUN yum -y update && yum -y upgrade &&\
  yum -y install wget gcc mesa-libGL libpng libSM libXrender libXdamage fontconfig librdmacm libibverbs &&\
  yum clean all

ENV LCG_BASE=/opt/lcg
ENV LCG_VERSION=101
ENV PYTHON_VERSION=3.9.6
ENV BINARY_TAG=x86_64-centos7-gcc11-opt

COPY lcg7-101.repo /etc/yum.repos.d/
RUN yum -y update && yum -y install LCG_101_Python_3.9.6_x86_64_centos7_gcc11_opt && yum clean all
RUN mkdir -p ${LCG_BASE}/releases
RUN ln -s ${LCG_BASE}/LCG_${LCG_VERSION}  ${LCG_BASE}/releases

ENV FELIX_DIST=https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist

# nightly
# ENV FELIX_URL=${FELIX_DIST}/software/nightly
# ENV FELIX_TARBALL=felix-master-rm5-stand-alone-x86_64-centos7-gcc11-opt.tar.gz
# ENV FELIX_BASE=/felix-master-rm5-stand-alone/x86_64-centos7-gcc11-opt

# tagged release
ENV FELIX_RELEASE=felix-05-00-00-rm5-stand-alone
ENV FELIX_TARBALL=${FELIX_RELEASE}-${BINARY_TAG}.tar.gz

ENV FELIX_BASE=/${FELIX_RELEASE}/${BINARY_TAG}

RUN wget -q ${FELIX_DIST}/software/apps/4.x/${FELIX_TARBALL} &&\
  tar -xzf ${FELIX_TARBALL} &&\
  rm ${FELIX_TARBALL}

SHELL ["/bin/bash", "--login", "-c"]

ENV HOME=/root
RUN touch ${HOME}/.bashrc
RUN echo ". ${FELIX_BASE}/setup.sh" >> ${HOME}/.bashrc
ENV LD_LIBRARY_PATH=/opt/lcg/LCG_101/Python/3.9.6/${BINARY_TAG}/lib/
